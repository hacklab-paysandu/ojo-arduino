#include <Servo.h>
#include <SoftwareSerial.h>

#define PIN_SER_0     12
#define PIN_SER_1     10
#define PIN_SER_2     9

#define PIN_RX        2
#define PIN_TX        3

#define ULTIMO_CHAR   '\n'

#define CANT_SERVOS   3

Servo servos[CANT_SERVOS];

SoftwareSerial Bluetooth ( PIN_RX, PIN_TX );

int posiciones[] = { -1, -1, -1 };

void setup ()
{
  Serial.begin( 9600 );
  Bluetooth.begin( 9600 );

  servos[0].attach( PIN_SER_0 );
  servos[1].attach( PIN_SER_1 );
  servos[2].attach( PIN_SER_2 );
}

String comando = "";
void loop ()
{
  if ( Bluetooth.available() )
  {
    char c = Bluetooth.read();

    if ( c == ULTIMO_CHAR )
    {
      procesarComando();
      comando = "";
    }
    else
      comando += c;
  }
}

/* función: procesarComando
 * 
 * servo:numero:angulo
 * todos:angulo_s1:angulo_s2
 * */
void procesarComando ()
{
  Serial.println( comando );
  
  String opcion = getFirst( comando );

  if ( opcion == "servo" )
  {
    String parametros = delFirst( comando );
    int numero, angulo;
    parametros > numero > angulo;

    if ( posiciones[numero] != angulo )
    {
      servos[numero].write( angulo );
      posiciones[numero] = angulo;
    }
  }
  else if ( opcion == "todos" )
  {
    String parametros = delFirst( comando );
    int angulo0, angulo1, angulo2;
    parametros > angulo0 > angulo1 > angulo2;

    if ( posiciones[0] != angulo0 )
    {
      servos[0].write( angulo0 );
      posiciones[0] = angulo0;
    }
    
    if ( posiciones[1] != angulo1 )
    {
      servos[1].write( angulo1 );
      posiciones[1] = angulo1;
    }
    
    if ( posiciones[2] != angulo2 )
    {
      servos[2].write( angulo2 );
      posiciones[2] = angulo2;
    }
  }
}
